//
//  ViewController.swift
//  HolyIAPExamples
//
//  Created by B-Arsekin on 11.08.2021.
//

import UIKit
import HolyIAP
import Purchases

final class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        HolyIAP.configure(revenueCatAPIKey: "", appsFlyerUID: "", configs: Test.shared, isDebug: false, isFakePurchaseOn: false)
        HolyIAP.shared.setupScreens(["1": IAPScreenModel(type: TestShopScreen.self, content: nil)])
    }
}

final class TestShopScreen: IAPViewController {
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    required init(iapManager: IAPManagable, iapContent: IAPContent?) {
        fatalError("init(iapManager:iapContent:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func fillPackages(_ packages: [Purchases.Package]) {
//        if let month = packages.first(where: { $0.packageType == .monthly }) {
//            print(month)
//        }
    }
}

final class Test: IAPConfigs {
    
    static let shared = Test()
    
    func getIAPConfigJSON(key: String) -> String {
        return ""
    }
}
