//
//  IAPViewController.swift
//  HolyIAP
//
//  Created by B-Arsekin on 16.08.2021.
//

import Foundation
import Purchases

open class IAPViewController: UIViewController, IAPScreen {
    public let iapManager: IAPManagable
    public let iapContent: IAPContent?
    public var selectedPackage: Purchases.Package?
    
    public var videoName: String = "" // deprecated
    public var videoURL: String {
        return iapManager.videoURL ?? ""
    }
    
    public var finished: ((Bool) -> Void)?
    
    required public init(iapManager: IAPManagable, iapContent: IAPContent?) {
        self.iapManager = iapManager
        self.iapContent = iapContent
        super.init(nibName: nil, bundle: nil)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        // TODO: analytics
        
        iapManager.getPackages { [weak self] packages in
            self?.selectedPackage = packages.first
            self?.fillPackages(packages)
        }
    }
    
    open func fillPackages(_ packages: [Purchases.Package]) {
        
    }
    
    open func close(purchased: Bool) {
        finished?(purchased)
        // TODO: analytics
    }
    
    open func purchasePressed() {
        guard let selectedPackage = selectedPackage ?? iapManager.packages.first else { return }
//        SVProgressHUD.show()
        iapManager.makePurchase(package: selectedPackage) { [weak self] purchased in
//            SVProgressHUD.dismiss()
            self?.close(purchased: purchased)
        }
    }
    
    open func restorePurchases() {
//        SVProgressHUD.show()
        iapManager.restorePurchase { [weak self] purchased in
//            SVProgressHUD.dismiss()
            self?.close(purchased: purchased)
        }
    }
    
    // MARK: - Actions
    @objc open func showPrivacy() {
//        let nav = UINavigationController(
//            rootViewController: WebViewController(url: URL(string: AppEnvironment.privacyPolicyURL)!))
//        present(nav, animated: true, completion: nil)
    }
    @objc open func showTOS() {
//        let nav = UINavigationController(
//            rootViewController: WebViewController(url: URL(string: AppEnvironment.termsOfUseURL)!))
//        present(nav, animated: true, completion: nil)
    }
}

public protocol HolyOnboardingAnalyticsProtocol {
    var analyticsHandler: OnboardingAnalyticsHandler? {get set}
}

public protocol OnboardingAnalyticsHandler: AnyObject {
    func sendAnalyticEvent(key: String, values: [String: Any])
    func sendAnalyticEvent(key: String, value: Any)
}
