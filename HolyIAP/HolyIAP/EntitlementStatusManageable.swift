//
//  EntitlementStatusManageable.swift
//  HolyIAP
//
//  Created by B-Arsekin on 16.08.2021.
//

import KeychainAccess

public protocol EntitlementStatusManageable {
    init(keychain: Keychain)
    
    func isEntitlementActive(_ entId: String) -> Bool
    func setEntitlementActive(_ entId: String)
    func setEntitlementNonActive(_ entId: String)
}

class KeychainEntManager: EntitlementStatusManageable {
    struct Keys {
        static let active = "1"
        static let nonActive = "0"
    }
    
    private let keychain: Keychain
    
    required init(keychain: Keychain = .init()) {
        self.keychain = keychain
    }
    
    public func isEntitlementActive(_ entId: String) -> Bool {
        return keychain[entId] == Keys.active
    }
    
    func setEntitlementActive(_ entId: String) {
        keychain[entId] = Keys.active
    }
    
    func setEntitlementNonActive(_ entId: String) {
        keychain[entId] = Keys.nonActive
    }
}
