//
//  HolyIAP.swift
//  HolyIAP
//
//  Created by B-Arsekin on 11.08.2021.
//

import Foundation
import Purchases
import KeychainAccess

open class HolyIAP {
    
    public static var shared: HolyIAP {
        get {
            if let shared = _shared {
                return shared
            }
            
            assert(_shared == nil, "Holy IAP is not configured")
            _shared = HolyIAP(configs: DefultIAPConfigs(), isFakePurhcaseOn: false)
            return _shared!
        }
    }
    
    let screenFactory: IAPScreenFactory
    public let entitlementManager: EntitlementStatusManageable
    public var isFakePurhcaseOn: Bool
    
    private static var _shared: HolyIAP?
    
    private init(configs: IAPConfigs, accessGroup: String? = nil, isFakePurhcaseOn: Bool) {
        let service = "holyiap"
        if let group = accessGroup {
            entitlementManager = KeychainEntManager(keychain: Keychain.init(service: service, accessGroup: group))
        } else {
            entitlementManager = KeychainEntManager(keychain: Keychain.init(service: service))
        }
        
        screenFactory = IAPScreenFactory(configs: configs, entManager: entitlementManager, isFakePurchaseOn: isFakePurhcaseOn)
        self.isFakePurhcaseOn = isFakePurhcaseOn
    }
    
    public static func configure(revenueCatAPIKey: String, appsFlyerUID: String, configs: IAPConfigs, isDebug: Bool, isFakePurchaseOn: Bool) {
        if isDebug {
            Purchases.logLevel = .debug
        }
        
        Purchases.configure(withAPIKey: revenueCatAPIKey)
        Purchases.shared.collectDeviceIdentifiers()
        Purchases.shared.setAppsflyerID(appsFlyerUID)
        
        _shared = HolyIAP(configs: configs, isFakePurhcaseOn: isFakePurchaseOn)
    }
    
    public func setupScreens(_ screens: [String: IAPScreenModel]) {
        screenFactory.setupScreens(screens)
    }
    
    public func changeIsFakePurchase(_ isOn: Bool) {
        isFakePurhcaseOn = isOn
        screenFactory.isFakePurchaseOn = isOn
    }
    
    public func getScreen(for place: String) -> IAPScreen? {
        return screenFactory.getScreen(for: place)
    }
}

public protocol IAPConfigs {
    func getIAPConfigJSON(key: String) -> String
}

class DefultIAPConfigs: IAPConfigs {
    func getIAPConfigJSON(key: String) -> String {
        return ""
    }
}

open class IAPContent {
    
}
