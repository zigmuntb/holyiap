//
//  IAPManager.swift
//  HolyIAP
//
//  Created by B-Arsekin on 16.08.2021.
//

import Foundation
import Purchases

public protocol IAPManagable {
    var place: String { get }
    var entId: String { get }
    var offeringId: String { get }
    var videoURL: String? { get }
    var packages: [Purchases.Package] { get }
    
    func getPackages(_ completion: @escaping ([Purchases.Package]) -> Void)
    func makePurchase(package: Purchases.Package, _ completion: @escaping (Bool) -> Void)
    func restorePurchase(_ completion: @escaping (Bool) -> Void)
}

class IAPManager: IAPManagable {
    let place: String
    let entId: String
    let offeringId: String
    let videoURL: String?
    
    private(set) var packages: [Purchases.Package] = []
    
    weak public var analytics: OnboardingAnalyticsHandler?
    
    private let isFakePurhcaseOn: Bool
    private let entManager: EntitlementStatusManageable
    
    init(place: String, entId: String, offeringId: String, entManager: EntitlementStatusManageable, videoURL: String?, isFakePurhcaseOn: Bool = false) {
        self.place = place
        self.entId = entId
        self.offeringId = offeringId
        self.entManager = entManager
        self.isFakePurhcaseOn = isFakePurhcaseOn
        self.videoURL = videoURL
    }
    
    func getPackages(_ completion: @escaping ([Purchases.Package]) -> Void) {
        Purchases.shared.offerings { [weak self] (offerings, error) in
            guard let self = self else {
                return
            }
            
            if let _ = error {
//                self.presentAlert(title: "Error".localized(), message: error.localizedDescription)
                completion([])
                return
            }
            
            guard let packages = offerings?.offering(identifier: self.offeringId)?.availablePackages else {
                return
            }
            
            self.packages = packages
            
            completion(packages)
        }
    }
    
    func makePurchase(package: Purchases.Package, _ completion: @escaping (Bool) -> Void) {
        
        if isFakePurhcaseOn {
            entManager.setEntitlementActive(entId)
            completion(true)
            return
        }
        
        Purchases.shared.purchasePackage(package) { [weak self, entManager] (_, purchaserInfo, error, cancelled) in
            guard let self = self else { return }
            
            func finish() {
                if let ent = purchaserInfo?.entitlements[self.entId] {
                    let id = ent.identifier
                    ent.isActive ? entManager.setEntitlementActive(id) : entManager.setEntitlementNonActive(id)
                    completion(ent.isActive)
                } else {
                    completion(false)
                }
            }
            
            guard !cancelled else {
                finish()
                return
            }
            
            if let error = error {
//                analytics.log(.error(message: error.localizedDescription, place: "inapp_pu.rchase"))
//                self.presentAlert(title: "Error".localized(), message: error.localizedDescription)
                print("Error making purchase: ", error.localizedDescription)
                completion(false)
                return
            }
            
            finish()
        }
    }
    
    func restorePurchase(_ completion: @escaping (Bool) -> Void) {
        Purchases.shared.restoreTransactions { [weak self, entManager] (info, error) in
            // save to holy iap entitlement
            guard let self = self else { return }
            
            if let ent = info?.entitlements[self.entId] {
                let id = ent.identifier
                ent.isActive ? entManager.setEntitlementActive(id) : entManager.setEntitlementNonActive(id)
                completion(ent.isActive)
            }
        }
    }
}
