//
//  IAPScreen.swift
//  HolyIAP
//
//  Created by B-Arsekin on 16.08.2021.
//

import UIKit

public struct IAPScreenModel {
    public let type: IAPScreen.Type
    public let content: IAPContent?
    
    public init(type: IAPScreen.Type, content: IAPContent?) {
        self.type = type
        self.content = content
    }
}

public protocol IAPScreen: UIViewController {
    var iapManager: IAPManagable { get }
    var iapContent: IAPContent? { get }
    init(iapManager: IAPManagable, iapContent: IAPContent?)
}
