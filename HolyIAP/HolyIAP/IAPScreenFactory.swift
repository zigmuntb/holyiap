//
//  IAPScreenFactory.swift
//  HolyIAP
//
//  Created by B-Arsekin on 16.08.2021.
//

import Foundation

open class IAPScreenFactory {
    struct IAPScreenConfig: Decodable {
        let identifier: String
        let offeringId: String
        let entId: String
        let videoUrl: String?
    }
    
    private var screens: [String: IAPScreenModel] = [:]
    private let configs: IAPConfigs
    private let entManager: EntitlementStatusManageable
    var isFakePurchaseOn: Bool
    
    init(configs: IAPConfigs, entManager: EntitlementStatusManageable, isFakePurchaseOn: Bool) {
        self.configs = configs
        self.entManager = entManager
        self.isFakePurchaseOn = isFakePurchaseOn
    }
    
    func setupScreens(_ screens: [String: IAPScreenModel]) {
        self.screens = screens
    }
    
    func getScreen(for place: String) -> IAPScreen? {
        if let config = getIAPConfig(place: place) {
            let screen = screens[config.identifier]
            return screen?.type.init(iapManager: IAPManager(place: place, entId: config.entId, offeringId: config.offeringId, entManager: entManager, videoURL: config.videoUrl, isFakePurhcaseOn: isFakePurchaseOn), iapContent: screen?.content)
        }
        
        return nil
    }
    
    private func getIAPConfig(place: String) -> IAPScreenConfig? {
        guard let data = configs.getIAPConfigJSON(key: place).data(using: .utf8) else { return nil }
        
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        do {
            let config = try decoder.decode(IAPScreenConfig.self, from: data)
            return config
        } catch {
            return nil
        }
    }
}
